#!/bin/bash

# Prompt the user to enter the resource group name
read -p "Please enter the resource group name: " RESOURCE_GROUP_NAME

# Execute the curl command to trigger the pipeline
curl --request POST \
     --header "PRIVATE-TOKEN: $GL_TOKEN" \
     --form "token=$GL_TOKEN" \
     --form "variables[][key]=RESOURCE_GROUP_NAME" \
     --form "variables[][value]=$RESOURCE_GROUP_NAME" \
     --form "ref=main" \
     "https://gitlab.com/api/v4/projects/55643992/pipeline" # Update this line to: https://gitlab.com/api/v4/projects/<your-project-number-here>/pipeline
