# GitLab Pipeline Automation Guide

This directory contains data regarding tests conducted with GitLab, specifically focusing on enabling external users to utilize GitLab pipelines and insert variables as per their requirements.

## Directory Contents

### main.tf and vars.tf

These files comprise straightforward Terraform code responsible for building a resource group in Azure. `vars.tf` hosts a single variable named "resource_group_name," which is subsequently integrated into the pipeline via the "$RESOURCE_GROUP_NAME" variable.

### .gitlab-ci.yml

This file encapsulates the essential data required for executing the pipeline.

### trigger_pipeline.sh

This script holds the necessary data for triggering the pipeline using the POST method by interacting directly with the GitLab API.

## Before you begin

Make sure you have created the SP in Azure and input ARM_CLIENT_ID, ARM_CLIENT_SECRET, ARM_SUBSCRIPTION_ID and ARM_TENANT_ID in your list of variables in GitLab.

## Running the Pipeline via the GitLab UI

Once you have copied the aforementioned files into your GitLab project, navigate to Build > Pipelines, and click "Run pipeline." In the "Variables" section, input the variable key as "RESOURCE_GROUP_NAME." Provide the desired value and click "Run pipeline" once more.

## Running the Pipeline via Shell Script

To trigger the pipeline using the shell script, navigate to your project directory in Linux or WSL, and execute the script with the following command:

```bash
./trigger_pipeline.sh
```

You'll be prompted to input the resource group name, after which the pipeline will be initiated. You can locate your project number by clicking the three dots in the upper right corner of the project screen. Additionally, consider generating a token solely for API interaction to restrict external access to the entire project.
